package matrix;

public class Matrix {

    public static void main(String[] args) {
        int[][] a = new int[3][5];

        ///////////////////////////// Fill array
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                a[i][j] = (int) (Math.random() * 100);
                System.out.print(a[i][j] + " ");
            }
            System.out.println("\n");
        }

        /////////////////////////////Find MIN & MAX values in each row
        int rowMin = 0;
        int rowMax = 0;
        int matrixMin = a[0][0];
        int matrixMax = a[0[0];
        int matrixMinIndex1 = 0;
        int matrixMinIndex2 = 0;
        int matrixMaxIndex1 = 0;
        int matrixMaxIndex2 = 0;

        for (int i = 0; i < a.length; i++) {
            rowMin = a[i][0];
            rowMax = a[i][0];
            for (int j = 0; j < a[i].length; j++) {
                if (a[i][j] < rowMin) {
                    rowMin = a[i][j];
                }
                if (a[i][j] > rowMax) {
                    rowMax = a[i][j];
                }
            }
            System.out.println("Row Number " + i + ": min = " + rowMin + ", max = " + rowMax);
        }
        ////////////////////////////////// Find MIN & MAX in matrix
        for (int i = 1; i < a.length; i++) {
            for (int j = 1; j < a[i].length; j++) {
                if (a[i][j] < matrixMin) {
                    matrixMin = a[i][j];
                    matrixMinIndex1 = i;
                    matrixMinIndex2 = j;
                }
                if (a[i][j] > matrixMax) {
                    matrixMax = a[i][j];
                    matrixMaxIndex1 = i;
                    matrixMaxIndex2 = j;
                }
            }
        }
        System.out.println("\n");
        System.out.println("Matrix MIN value a[" + matrixMinIndex1 + "][" + matrixMinIndex2 + "] = " + matrixMin);
        System.out.println("Matrix MAX value a[" + matrixMaxIndex1 + "][" + matrixMaxIndex2 + "] = " + matrixMax);

        
        //////////////////////////////// Change indexes of MIN & MAX values, and print new matrix
        a[matrixMinIndex1][matrixMinIndex2] = matrixMax;
        a[matrixMaxIndex1][matrixMaxIndex2] = matrixMin;
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println("\n");
        }
    }
};
